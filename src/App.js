import React from "react";
import Form from "./components/form.tsx";

const App = () => {
  return (
    <div>
      <Form />
    </div>
  );
};

export default App;
