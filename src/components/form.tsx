import React, { useEffect, useState } from "react";
import { useFormik, FormikHelpers } from "formik";
import * as Yup from "yup";
import {
  TextField,
  Button,
  InputAdornment,
  Typography,
  Modal,
  IconButton,
  Divider,
  Snackbar,
  List,
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
} from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import Autocomplete from "@mui/material/Autocomplete";
import Axios from "axios";
import { Close, Delete } from "@mui/icons-material";

interface FormValues {
  searchText: string;
  textboxValue: string;
}

interface Flag {
  name: string;
  flag: string;
  reason: string;
}

const FormComponent: React.FC = () => {
  const [cityNames, setCityNames] = useState<Flag[]>([]);
  const [open, setOpen] = useState(false);
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const [flags, setFlags] = useState<Flag[]>(() => {
    const storedFlags = localStorage.getItem("flags");
    return storedFlags ? JSON.parse(storedFlags) : [];
  });

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleSnackbarClose = () => {
    setSnackbarOpen(false);
  };

  useEffect(() => {
    const fetchCityNames = async () => {
      try {
        const response = await Axios.get("https://restcountries.com/v3.1/all");
        const data = response.data;
        const cities: Flag[] = [];

        data.forEach((country: any) => {
          cities.push({
            name: country.name.common,
            flag: country.flags.svg,
            reason: formik.values.textboxValue,
          });
        });

        setCityNames(cities);
      } catch (error) {
        console.error("Error fetching city names:", error);
      }
    };

    fetchCityNames();
  }, []);

  const formik = useFormik<FormValues>({
    initialValues: {
      searchText: "",
      textboxValue: "",
    },
    validationSchema: Yup.object({
      searchText: Yup.string().required("Flag is required"),
      textboxValue: Yup.string()
        .required("Reason is required")
        .min(15, "Reason should be at least 15 characters")
        .max(255, "Reason should be at most 255 characters"),
    }),
    onSubmit: (values: FormValues, helpers: FormikHelpers<FormValues>) => {
      const newFlag: Flag = {
        name: values.searchText,
        flag:
          cityNames.find((city) => city.name === values.searchText)?.flag || "",
        reason: values.textboxValue,
      };

      setFlags((prevFlags) => [...prevFlags, newFlag]);
      helpers.resetForm();
    },
  });

  const handleAddButtonClick = () => {
    if (!formik.values.searchText || !formik.values.textboxValue) {
      setSnackbarOpen(true);
    } else {
      formik.handleSubmit();
    }
  };

  useEffect(() => {
    localStorage.setItem("flags", JSON.stringify(flags));
  }, [flags]);

  const handleDeleteClick = (index: number) => {
    const updatedFlags = [...flags];
    updatedFlags.splice(index, 1);
    setFlags(updatedFlags);
  };

  return (
    <div className="App">
      <Button
        variant="contained"
        color="primary"
        style={{
          position: "fixed",
          top: "50%",
          left: "50%",
          transform: "translate(-50%, -50%)",
        }}
        onClick={handleOpen}
      >
        Show Add Flag Modal
      </Button>
      <Modal open={open} onClose={handleClose}>
        <div
          className="modal"
          style={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            width: "400px",
            background: "white",
            padding: "20px",
            borderRadius: "8px",
          }}
        >
          <div
            style={{
              display: "flex",
              alignItems: "center",
              marginBottom: "16px",
            }}
          >
            <Typography
              variant="h6"
              component="h2"
              style={{
                color: "#680daf",
                fontWeight: "bold",
                marginRight: "8px",
              }}
            >
              Add New Flag
            </Typography>
            <IconButton
              aria-label="close"
              color="error"
              onClick={handleClose}
              style={{ marginLeft: "auto" }}
            >
              <Close />
            </IconButton>
          </div>
          <Divider style={{ marginBottom: "16px" }} />
          <form onSubmit={formik.handleSubmit}>
            <Autocomplete
              freeSolo
              options={cityNames.map((city) => city.name)}
              value={formik.values.searchText}
              onChange={(event, value) => {
                formik.setFieldValue("searchText", value || "");
              }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  id="searchText"
                  name="searchText"
                  label={
                    <React.Fragment>
                      Select a flag<span style={{ color: "red" }}>*</span>
                    </React.Fragment>
                  }
                  value={formik.values.searchText}
                  onChange={formik.handleChange}
                  error={
                    formik.touched.searchText &&
                    Boolean(formik.errors.searchText)
                  }
                  helperText={
                    formik.touched.searchText && formik.errors.searchText
                  }
                  InputProps={{
                    ...params.InputProps,
                    endAdornment: (
                      <InputAdornment position="end">
                        <SearchIcon style={{ marginRight: "-28px" }} />
                      </InputAdornment>
                    ),
                  }}
                  fullWidth
                  margin="normal"
                />
              )}
            />
            <Divider style={{ margin: "16px 0" }} />
            <TextField
              id="textboxValue"
              name="textboxValue"
              label={
                <React.Fragment>
                  Reason<span style={{ color: "red" }}>*</span>
                </React.Fragment>
              }
              multiline
              rows={4}
              value={formik.values.textboxValue}
              onChange={formik.handleChange}
              error={
                formik.touched.textboxValue &&
                Boolean(formik.errors.textboxValue)
              }
              helperText={
                formik.touched.textboxValue && formik.errors.textboxValue
              }
              fullWidth
              margin="normal"
            />
            <Divider style={{ margin: "16px 0" }} />
            <div style={{ display: "flex", justifyContent: "flex-end" }}>
              <Button
                variant="contained"
                color="secondary"
                onClick={formik.handleReset}
                style={{
                  color: "#680daf",
                  border: "1px solid #680daf",
                  backgroundColor: "white",
                }}
              >
                Cancel
              </Button>
              <Button
                type="button"
                variant="contained"
                color="primary"
                onClick={handleAddButtonClick}
                style={{
                  backgroundColor: "#680daf",
                  color: "white",
                  marginLeft: "8px",
                }}
              >
                Add
              </Button>
            </div>
          </form>
        </div>
      </Modal>
      <Divider style={{ margin: "16px 0" }} />
      <List>
        {flags.map((flag, index) => (
          <ListItem key={index}>
            <img
              src={flag.flag}
              alt={flag.name}
              style={{ width: "30px", height: "20px", marginRight: "16px" }}
            />
            <ListItemText primary={flag.name} />
            <ListItemText style={{ color: "gray" }} primary={flag.reason} />
            <ListItemSecondaryAction>
              <IconButton
                edge="end"
                aria-label="delete"
                onClick={() => handleDeleteClick(index)}
              >
                <Delete />
              </IconButton>
            </ListItemSecondaryAction>
          </ListItem>
        ))}
      </List>
      <Snackbar
        open={snackbarOpen}
        autoHideDuration={4000}
        onClose={handleSnackbarClose}
        message="Flag and reason are required"
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
      />
    </div>
  );
};

export default FormComponent;
